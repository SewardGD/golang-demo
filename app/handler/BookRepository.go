package handler

import (
	"MyWorkSpace/Golang-Demo/app/conf"
	"MyWorkSpace/Golang-Demo/app/models"
	"database/sql"
	_ "fmt"
	"log"
)

func GetAll() ([]models.Book, error) {
	db := conf.Config()
	defer db.Close()
	sqlStatement := `SELECT id,title,total_pages,rating,isbn,author FROM books`
	rows, err := db.Query(sqlStatement)

	if err != nil {
		panic(err)
	}
	defer rows.Close()
	result := []models.Book{}

	for rows.Next() {
		book := models.Book{}
		err2 := rows.Scan(&book.Id, &book.Title, &book.TotalPages, &book.Rating, &book.Isbn, &book.Author)
		if err2 != nil {
			return nil, err2
		}
		result = append(result, book)
	}
	return result, nil

}

func GetById(id int64) (models.Book, error) {
	db := conf.Config()
	defer db.Close()
	sqlStatement := `SELECT id,title,total_pages,rating,isbn,author FROM books WHERE id=$1`
	book := models.Book{}
	row := db.QueryRow(sqlStatement, id)
	err := row.Scan(&book.Id, &book.Title, &book.TotalPages, &book.Rating, &book.Isbn, &book.Author)

	if err == sql.ErrNoRows {
		log.Println("No rows were returned!")
	}
	if err != nil {
		return models.Book{}, err
	}
	return book, nil
}

func Insert(book *models.Book) error {
	db := conf.Config()
	defer db.Close()

	sqlStatement := `INSERT INTO books (title, total_pages, rating, isbn, author, updated_at, created_at) VALUES ($1, $2, $3, $4, $5, $6, $7)`
	_, err := db.Exec(sqlStatement, book.Title, book.TotalPages, book.Rating, book.Isbn, book.Author, book.UpdatedAt, book.CreatedAt)

	if err != nil {
		return err
	}
	return nil
}

func Update(book *models.Book, id int64) error {
	db := conf.Config()
	defer db.Close()

	sqlStatement := `UPDATE books SET title=$2, total_pages=$3, rating=$4, isbn=$5, author=$6, updated_at=$7, created_at=$8 WHERE id=$1`
	_, err := db.Exec(sqlStatement, id, book.Title, book.TotalPages, book.Rating, book.Isbn, book.Author, book.UpdatedAt, book.CreatedAt)

	if err != nil {
		return err
	}
	return nil
}

func Delete(id int64) error {
	db := conf.Config()
	defer db.Close()

	sqlStatement := `DELETE FROM books WHERE id=$1`
	_, err := db.Exec(sqlStatement, id)

	if err != nil {
		return err
	}
	return nil
}
