package models

import "time"

// Book struct (Model)
type Book struct {
	Id     int64       `json:"id"`
	Title  string       `json:"title"`
	TotalPages   string `json:"total_pages"`
	Rating   string     `json:"rating"`
	Isbn   string       `json:"isbn"`
	Author string      `json:"author"`
	UpdatedAt time.Time `json:"updated_at"`
	CreatedAt time.Time `json:"created_at"`
}

func NewBook(title string, totalPages string, rating string, isbn string, author string) *Book {
	return &Book{
		Title: title,
		TotalPages: totalPages,
		Rating: rating,
		Isbn: isbn,
		Author: author,
		UpdatedAt: time.Now(),
		CreatedAt: time.Now(),
	}
}

type Books struct {
	Books []Book `json:"books"`
}

func (book *Book) GetId() int64 {
	return book.Id
}

func (book *Book) GetTitle() string {
	return book.Title
}

func (book *Book) GetTotalPages() string {
	return book.TotalPages
}

func (book *Book) GetRating() string {
	return book.Rating
}

func (book *Book) GetIsbn() string {
	return book.Isbn
}

func (book *Book) GetAuthor() string {
	return book.Author
}

func (book *Book) SetId(id int64) {
	book.Id = id
}

func (book *Book) SetTitle(title string) {
	book.Title = title
}

func (book *Book) SetTotalPages(totalPages string) {
	book.TotalPages = totalPages
}

func (book *Book) setRating(rating string) {
	book.Rating = rating
}

func (book *Book) setIsbn(isbn string) {
	book.Isbn = isbn
}

func (book *Book) setAuthor(author string) {
	book.Author = author
}
