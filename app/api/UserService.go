package api

import (
	"MyWorkSpace/Golang-Demo/app/handler"
	"MyWorkSpace/Golang-Demo/app/models"
	"fmt"
	"github.com/labstack/echo"
	"net/http"
)

func Signup(ctx echo.Context) error {
	user := models.NewUser()
	if err := ctx.Bind(user); err != nil {
		return err
	}
	
	//Validate
	if user.Email == "" || user.UserName == "" || user.Password == "" {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "invalid email or username or password"}
	}

	err := handler.Signup(user)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return ctx.String(http.StatusOK, "ok")
}

func Login(ctx echo.Context) error  {
	//user := models.NewUser()
	//if err := ctx.Bind(user); err != nil {
	//	return err
	//}
	//
	////Validate
	//if user.Email == "" || user.Password == "" {
	//	return &echo.HTTPError{Code: http.StatusBadRequest, Message: "invalid email or password"}
	//}

	//Find user
	result, err := handler.Login("seward@gmail.com", "Hainguyen")
	if err != nil {
		fmt.Println(err)
		return err
	}
	return ctx.JSON(http.StatusCreated, result.UserName)
}